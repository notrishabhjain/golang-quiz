package main

// Program to pick random numbers for maxTurns times and user needs to guess the number in form of
// command line argument to see if they win or not. A special msg if user wins in first attempt.
import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	maxTurns = 10
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Println("Enter the number for guessing")
		return
	}

	guess, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Enter a correct number")
		return
	}

	if guess < 0 {
		fmt.Println("Enter a positive number ")
		return
	}

	rand.Seed(time.Now().UnixNano())

	for turn := 1; turn <= maxTurns; turn++ {
		n := rand.Intn(guess + 1)
		//fmt.Printf("%d ", n)
		if n != guess {
			continue
		}
		if turn == 1 {
			fmt.Println("Won in first turn")
		} else {
			fmt.Println("You won")
		}
		return
	}
	fmt.Println("You lose")

}
